import 'package:basic_shape_drawer/service/draw_service.dart';
import 'package:basic_shape_drawer/service/polygon.dart';
import 'package:basic_shape_drawer/service/shape.dart';
import 'package:basic_shape_drawer/view/widgets/main_painter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'basic_shape_drawer',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'basic_shape_drawer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DrawService _drawService = DrawService.instance();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeRight, DeviceOrientation.landscapeLeft]);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: OrientationBuilder(
          builder: (context, o) => CustomPaint(
            painter: MainPainter(_drawService),
            isComplex: true,
            willChange: true,
            size: MediaQuery.of(context).size,
            child: GestureDetector(
              child: Container(
                color: Colors.transparent,
              ),
              onTapUp: (details) {
                Shape shape = new PolygonShape(
                  points: [
                    details.localPosition,
                    Offset(details.localPosition.dx + 30,
                        details.localPosition.dy),
                    Offset(details.localPosition.dx + 30,
                        details.localPosition.dy + 30),
                    Offset(details.localPosition.dx,
                        details.localPosition.dy + 30),
                    details.localPosition,
                  ],
                  color: Colors.black,
                );
                _drawService.addShape(shape);
                setState(() {});
              },
              onLongPressEnd: (details) {
                Shape shape = new PolygonShape(
                  points: [
                    details.localPosition,
                    details.localPosition.translate(
                        details.localPosition.dx + 10,
                        details.localPosition.dy + 10)
                  ],
                  color: Colors.red,
                );
                _drawService.addShape(shape);
                setState(() {});
              },
            ),
          ),
        ));
  }
}

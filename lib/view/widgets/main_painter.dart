import 'dart:ui';

import 'package:basic_shape_drawer/service/draw_service.dart';
import 'package:basic_shape_drawer/service/polygon.dart';
import 'package:flutter/widgets.dart';

class MainPainter extends CustomPainter {
  final DrawService _service;

  MainPainter(DrawService service) : _service = service;

  @override
  void paint(Canvas canvas, Size size) {
    _service.shapes.forEach((key, value) {
      if (value is PolygonShape) {
        canvas.drawPoints(value.pointMode, value.points, value.paint);
      }
    });
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

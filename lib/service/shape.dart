import 'package:flutter/widgets.dart';

abstract class Shape {
  final String id;
  final Paint paint;

  Shape(this.id, this.paint);
}

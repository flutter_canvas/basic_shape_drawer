import 'package:basic_shape_drawer/service/shape.dart';
import 'package:flutter/foundation.dart';

class DrawService with ChangeNotifier {
  Map<String, Shape> _shapes;

  static DrawService _service = DrawService._internal();

  DrawService._internal() : _shapes = {};

  factory DrawService.instance() => _service;

  Map<String, Shape> get shapes => _shapes;
  void addShape(Shape shape) async {
    if (shape != null) {
      String id = await _generatShapeId();

      _shapes[id] = shape;
      notifyListeners();
    }
  }

  Future<String> _generatShapeId() async {
    String id;
    for (int i = 0; i <= _shapes.length; i++) {
      id = "shape_" + i.toString();
      if (!_shapes.containsKey(id)) {
        return id;
      }
    }
    return id;
  }

  void deleteShape(String id) {
    _shapes.removeWhere((key, value) => key == id);
    notifyListeners();
  }
}

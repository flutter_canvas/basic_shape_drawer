import 'dart:ui';

import 'package:basic_shape_drawer/service/shape.dart';

class Shape2D extends Shape {
  final PointMode pointMode;
  final List<Offset> points;
  final String id;

  Shape2D(this.pointMode, this.points, Paint paint, this.id) : super(id, paint);
}

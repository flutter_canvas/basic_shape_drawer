import 'dart:ui';

import 'package:basic_shape_drawer/service/2d_shape.dart';
import 'package:flutter/foundation.dart';

class PolygonShape extends Shape2D {
  List<Offset> points;
  Color color;
  Paint paint;
  final String id;
  final PolygonType type;

  PolygonShape({@required List<Offset> points, this.color, this.type, this.id})
      : this.points = points,
        paint = _configurePaint(color),
        super(PointMode.polygon, points, _configurePaint(color), id);

  static Paint _configurePaint(Color color) {
    Paint paint = Paint();
    paint.color = color;
    return paint;
  }
}

enum PolygonType { rectangle, triangle, other }
